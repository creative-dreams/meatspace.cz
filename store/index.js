import Vuex from 'vuex'

const createStore = () => {
	return new Vuex.Store({
		state: {
			loadedDataPlaces: [],
			visitors: ['Libovolný', 'pro 30 hostů', 'pro 60 hostů', 'pro 120 hostů', 'pro 200 hostů', 'pro 500 hostů', 'pro více jak 500 hostů'],
			cities: ['Praha', 'Brno', 'Ostrava', 'Plzeň', 'České Budějovice', 'Ostatní lokality'],
			eventType: ['meeting', 'indoor', 'party', 'hobby', 'ostatni'],
			labelCityFilters: [
				{ city: 'Praha', active: false },
				{ city: 'Brno', active: false },
				{ city: 'Ostrava', active: false },
				{ city: 'Plzeň', active: false },
				{ city: 'České Budějovice', active: false },
				{ city: 'Ostatní lokality', active: false },
			],
			hashtagFilters: [
				{ hashtag: 'meeting', active: false },
				{ hashtag: 'indoor', active: false },
				{ hashtag: 'party', active: false },
				{ hashtag: 'hobby', active: false }
			]
		},
		mutations: {
			setLoadedDataPlaces (state, payload) {
				state.loadedDataPlaces = payload
			}
		},
		actions: {
			loadDataPlaces({ commit }) {
				this.$axios.get('https://jsonplaceholder.typicode.com/photos').then((response) => {
					commit('setLoadedDataPlaces', response.data)
				})
			}
		},
		getters: {
			loadedDataPlaces(state) {
				return state.loadedDataPlaces
			},
			cityFilters(state) {
				return state.labelCityFilters
			},
			hashtagFilters(state) {
				return state.hashtagFilters
			},
			visitors(state) {
				return state.visitors
			},
			cities(state) {
				return state.cities
			},
			eventType(state) {
				return state.eventType
			}
		}
	})
}

export default createStore